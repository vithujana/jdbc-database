package dbconnection2;

import java.sql.Connection;
import java.sql.SQLException;

public class JDBCDemo {
	
	public static void main(String[] args) {
		JDBC jdbc = JDBC.getInstance();
		
		try {
			Connection con = jdbc.getConnection();
			System.out.println("connected");
			System.out.println(con);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}catch (SQLException e) {
			e.printStackTrace();	
		}
	}
}